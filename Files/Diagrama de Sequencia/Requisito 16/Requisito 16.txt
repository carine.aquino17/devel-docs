@startuml

autonumber
actor Jogador
boundary Cliente

Jogador -> Cliente : Digita mensagem textual no bate papo.
Cliente -> Cliente: Pega o Id e a mensagem do Jogador.
Cliente -> Cliente: Gera o comando de envio: pline < iDJogador > < mensagem >
Cliente -> Cliente: Converte a mensagem para Hexadecimal.
Cliente -> Servidor : Envia mensagem para o servidor, usando o Protocolo TCP.
Servidor -> Servidor: Recebe a mensagem e prepara para encaminhar para todos os clientes ativos.
Servidor -> Cliente : Envia mensagem para todos os clientes usando a resposta: pline < iDJogador > < mensagem > em formato Hexadecimal.
Cliente -> Cliente : Traduz a mensagem de Hexadecimal para String.
Cliente -> Jogador: Exibe as informações(Nickname do jogador de origem e mensagem enviada) da mensagem no bate papo.
@enduml
