﻿
#  Requisito 16 - Bate Papo Canal Principal

## 1 - Resumo
**História de Usuário**
>Como jogador, gostaria de conversar em um bate-papo geral com outros jogadores.

**Pré-condições**
>O jogador deve possuir um nickname e deve estar online.

**Pós-condições**
>1. **Fluxo Normal** <br />
    1.1. O usuário digita uma mensagem textual e envia no bate-papo;<br />
    1.2. O Servidor recebe essa mensagem e e envia para todos os jogadores que estiverem online.<br />

>2. **Fluxo Alternativo** <br />
    2.1. A mensagem não chega no servidor;  <br />
    2.2. O servidor não encaminha a mensagem para os jogadores;<br />
    2.3. Um jogador que não esteja online, não receberá essa mensagem.<br />

**Observações**
>Os jogadores tem opção de interagir entre si através de trocas de mensagens textuais no bate papo do canal principal.
## 2 - Desenvolvedor
**Protocolo**
>TCP

**Mensagens**
###### Observação: Todas as mensagens são enviadas no formato Hexadecimal.

>1. **Enviadas para o Servidor** <br/>
> --- **Codificação**: Essa mensagem **não é codificada**!<br/>
> --- **Mensagem enviada**: **pline < iDJogador > < mensagem >**<br/>
> --- **Parâmetros**:<br/>
> --------- iDJogador:number => Id do jogador que enviou a mensagem;<br/>
> --------- mensagem:string => Mensagem a ser enviada;<br/>
   
> **Exemplo de envio**:<br/>
 > --------- em **Hexadecimal**: <br/>706c696e652031206f6ce12c207475646f20626f6d3f**ff** (Ao final da string adiciona 'ff')<br/>
> --------- em **Texto imprimivel**: pline 1 olá, tudo bom?<br/>

>2. **Recebidas do Servidor** <br/>
>**Observação**: O servidor envia essa mensagem para todos os clientes ativos, através de **comunicação sockets**<br/>
> --- **Codificação**: Essa mensagem **não é codificada**!<br/>
> --- **Mensagem enviada**: **pline < iDJogador > < mensagem >**<br/>
> --- **Parâmetros**:<br/>
> --------- iDJogador:number => Id do jogador que enviou a mensagem;<br/>
> --------- mensagem:string => Mensagem a ser enviada;<br/>
> ---  **Exemplo de envio**:<br/>
 > --------- em **Hexadecimal**: 706c696e652031206f6ce12c207475646f20626f6d3f<br/>
> --------- em **Texto imprimivel**: pline 1 olá, tudo bom?   <br/>
## 3 - Diagrama de sequência

![Requisito 16](https://gitlab.com/tetrinetjs/devel-docs/raw/jonathas/Files/Diagrama%20de%20Sequencia/Requisito%2016/Requisito%2016%20-%20Bate%20Papo%20Canal%20Principal.png)



