﻿
#  encode();

**Função usada para codificar uma mensagem.**
>Exemplo de implementação na linguagem Java:

    /**
     * Return the initialization string for the specified user.
     *
     * @param nickname  the nickname of the client
     * @param version   the version of the client
     * @param ip        the IP of the server
     * @param tetrifast is this a tetrifast client ?
     */
    public static String encode(String nickname, String version, byte[] ip, boolean tetrifast)
    {
        // compute the pattern
        int p = 54 * ip[0] + 41 * ip[1] + 29 * ip[2] + 17 * ip[3];
        char[] pattern = String.valueOf(p).toCharArray();

        // build the string to encode
        char[] data = ((tetrifast ? "tetrifaster " : "tetrisstart ") + nickname + " " + version).toCharArray();

        // build the encoded string
        StringBuffer result = new StringBuffer();
        char offset = 0x80;
        result.append(toHex(offset));

        char previous = offset;

        for (int i = 0; i < data.length; i++)
        {
            char current = (char) (((previous + data[i]) % 255) ^ pattern[i % pattern.length]);
            result.append(toHex(current));
            previous = current;
        }

        return result.toString().toUpperCase();
    }

**Função auxiliar do Encode().**
>Exemplo de implementação na linguagem Java:

    /**
     * Return the hex value of the specified byte on 2 digits.
     */
    private static String toHex(char c)
    {
        String h = Integer.toHexString(c);

        return h.length() > 1 ? h : "0" + h;
    }
